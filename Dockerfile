FROM java:8

# Install maven
RUN apt-get update
RUN apt-get install -y maven

# install from nodesource using apt-get
# https://www.digitalocean.com/community/tutorials/how-to-install-node-js-on-an-ubuntu-14-04-server
RUN curl -sL https://deb.nodesource.com/setup | bash - && apt-get install -yq nodejs build-essential

# install grunt-cli
RUN npm install -g yo grunt-cli

WORKDIR /code

RUN mkdir /code/.stormpath
ADD ./.stormpath/apiKey.properties /code/.stormpath/apiKey.properties

# Prepare
ADD pom.xml /code/pom.xml
ADD package.json /code/package.json
ADD Gruntfile.js /code/Gruntfile.js

# Adding source, compile and package into a fat jar
ADD src /code/src
RUN ["mvn", "-B", "-DskipTests=true", "clean", "install"]

# Compile and package into a fat jar
RUN ["mvn", "package"]

ENV PORT 8000
ENV TRUSTED_SERVER true
ENV STORMPATH_API_KEY_FILE /code/.stormpath/apiKey.properties
ENV _JAVA_OPTIONS -Djava.security.egd=file:///dev/urandom

EXPOSE 8000

WORKDIR target
CMD ["java", "-jar", "springboot-starter-0.1.0.jar"]