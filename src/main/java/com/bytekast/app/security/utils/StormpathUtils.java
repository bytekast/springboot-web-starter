package com.bytekast.app.security.utils;

import com.bytekast.app.models.Todo;
import com.stormpath.sdk.account.Account;
import com.stormpath.sdk.application.Application;
import com.stormpath.sdk.cache.Caches;
import com.stormpath.sdk.client.Client;
import com.stormpath.sdk.client.Clients;
import com.stormpath.sdk.directory.CustomData;
import com.stormpath.sdk.servlet.account.AccountResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author Rowell Belen
 */
@Component
public class StormpathUtils {

  @Autowired
  private Application application;

  @Autowired
  private Client client;

  private Client cacheDisabledClient;

  @PostConstruct
  public void init() {
    cacheDisabledClient = Clients.builder().setCacheManager(
       Caches.newDisabledCacheManager()
    ).build();
  }

  public Account getAuthenticatedAccount(HttpServletRequest request) {
    if (AccountResolver.INSTANCE.hasAccount(request)) {
      return AccountResolver.INSTANCE.getRequiredAccount(request);
    }
    return null;
  }

  public boolean isMemberOfGroup(Account account, String groupName) {
    return account.isMemberOfGroup(groupName);
  }

  public void saveTodos(final Account account, final List<Todo> todoList) {
    CustomData customData = client.getResource(account.getCustomData().getHref(), CustomData.class);
    customData.put("todos", todoList);
    customData.save();
  }

  public List<Todo> getTodos(final Account account) {
    CustomData customData = cacheDisabledClient.getResource(account.getCustomData().getHref(), CustomData.class);
    return (List<Todo>) customData.get("todos");
  }

  public Application getApplication() {
    return application;
  }

  public Client getClient() {
    return client;
  }
}
