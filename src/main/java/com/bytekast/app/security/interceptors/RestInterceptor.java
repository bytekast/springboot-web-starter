package com.bytekast.app.security.interceptors;

import com.bytekast.app.security.utils.StormpathUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author Rowell Belen
 */
@Component
public class RestInterceptor extends HandlerInterceptorAdapter {

  @Autowired
  private StormpathUtils stormpathUtils;

  @Override
  public boolean preHandle(HttpServletRequest request,
                           HttpServletResponse response, Object handler) throws Exception {
    if (((HandlerMethod) handler).getMethod().isAnnotationPresent(AuthorizationRequired.class)) {
      AuthorizationRequired authorized = ((HandlerMethod) handler).getMethod().getAnnotation(AuthorizationRequired.class);
      if (authorized != null) {
        if (stormpathUtils.getAuthenticatedAccount(request) != null) {
          return true;
        }
        else {
          response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
          return false;
        }
      }
    }

    return true;
  }
}
