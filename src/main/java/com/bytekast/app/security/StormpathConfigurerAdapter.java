package com.bytekast.app.security;

import com.bytekast.app.security.filters.AuthenticationFilter;
import com.bytekast.app.security.filters.ForceSSLFilter;
import com.bytekast.app.security.filters.ManageAccessFilter;
import com.bytekast.app.security.interceptors.RestInterceptor;
import com.bytekast.app.security.utils.StormpathUtils;
import com.stormpath.sdk.servlet.http.Resolver;
import com.stormpath.sdk.servlet.util.IsLocalhostResolver;
import com.stormpath.sdk.servlet.util.RemoteAddrResolver;
import com.stormpath.sdk.servlet.util.SecureRequiredExceptForLocalhostResolver;
import org.apache.commons.lang.BooleanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import javax.servlet.DispatcherType;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;
import java.util.EnumSet;

/**
 * @author Rowell Belen
 */
@Configuration
public class StormpathConfigurerAdapter extends WebMvcConfigurerAdapter {

  @Autowired
  private RestInterceptor restInterceptor;

  @Autowired
  private StormpathUtils stormpathUtils;

  @Override
  public void addInterceptors(InterceptorRegistry registry) {

    registry.addInterceptor(restInterceptor)
       .addPathPatterns("/user/**");

    super.addInterceptors(registry);
  }

  @Bean
  public FilterRegistrationBean forceSslFilter() {
    EnumSet<DispatcherType> types = EnumSet.noneOf(DispatcherType.class);
    types.addAll(Arrays.asList(DispatcherType.values()));

    FilterRegistrationBean registrationBean = new FilterRegistrationBean();
    registrationBean.setFilter(new ForceSSLFilter());
    registrationBean.setName("forceSSLFilter");
    registrationBean.setDispatcherTypes(types);
    registrationBean.addUrlPatterns("/*");
    registrationBean.setOrder(0);

    return registrationBean;
  }

  @Bean
  public FilterRegistrationBean authenticationFilter() {
    EnumSet<DispatcherType> types = EnumSet.noneOf(DispatcherType.class);
    types.addAll(Arrays.asList(DispatcherType.values()));

    FilterRegistrationBean registrationBean = new FilterRegistrationBean();
    registrationBean.setFilter(new AuthenticationFilter(stormpathUtils));
    registrationBean.setName("authenticationFilter");
    registrationBean.setDispatcherTypes(types);
    registrationBean.addUrlPatterns("/*");
    registrationBean.setOrder(0);

    return registrationBean;
  }

  @Bean
  public FilterRegistrationBean manageAccessFilter() {
    EnumSet<DispatcherType> types = EnumSet.noneOf(DispatcherType.class);
    types.addAll(Arrays.asList(DispatcherType.values()));

    FilterRegistrationBean registrationBean = new FilterRegistrationBean();
    registrationBean.setFilter(new ManageAccessFilter(stormpathUtils));
    registrationBean.setName("manageAccessFilter");
    registrationBean.setDispatcherTypes(types);
    registrationBean.addUrlPatterns("/manage/*");
    registrationBean.setOrder(0);

    return registrationBean;
  }

  @Bean
  public Resolver<Boolean> stormpathSecureResolver() {
    return new Resolver<Boolean>() {
      @Override
      public Boolean get(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) {

        // Allow non-https connections if TRUSTED_SERVER_ENV is true
        if (BooleanUtils.toBoolean(System.getenv("TRUSTED_SERVER"))) {
          return false;
        }

        // Default
        SecureRequiredExceptForLocalhostResolver resolver =
           new SecureRequiredExceptForLocalhostResolver(new IsLocalhostResolver(new RemoteAddrResolver()));
        return resolver.get(httpServletRequest, httpServletResponse);
      }
    };
  }
}


