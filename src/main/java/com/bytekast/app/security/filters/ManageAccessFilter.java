package com.bytekast.app.security.filters;

import com.bytekast.app.security.utils.StormpathUtils;
import com.stormpath.sdk.account.Account;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author Rowell Belen
 */
public class ManageAccessFilter implements Filter {

  private StormpathUtils stormpathUtils;

  public ManageAccessFilter(StormpathUtils stormpathUtils) {
    this.stormpathUtils = stormpathUtils;
  }

  @Override
  public void init(FilterConfig filterConfig) throws ServletException {
    // do nothing
  }

  @Override
  public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
    HttpServletRequest request = (HttpServletRequest) servletRequest;
    HttpServletResponse response = (HttpServletResponse) servletResponse;

    Account account = stormpathUtils.getAuthenticatedAccount(request);
    if (account != null && stormpathUtils.isMemberOfGroup(account, "admin")) {
      filterChain.doFilter(request, response);
      return;
    }

    response.sendRedirect("/login?next=" + request.getRequestURI()); // redirect to login page
  }

  @Override
  public void destroy() {
    // do nothing
  }
}
