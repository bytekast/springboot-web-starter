package com.bytekast.app.security.filters;

import com.bytekast.app.security.utils.StormpathUtils;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author Rowell Belen
 */
public class AuthenticationFilter implements Filter {

  private StormpathUtils stormpathUtils;

  public AuthenticationFilter(StormpathUtils stormpathUtils) {
    this.stormpathUtils = stormpathUtils;
  }


  @Override
  public void init(FilterConfig filterConfig) throws ServletException {
    // do nothing
  }

  @Override
  public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {

    HttpServletRequest request = (HttpServletRequest) servletRequest;
    HttpServletResponse response = (HttpServletResponse) servletResponse;

    if (stormpathUtils.getAuthenticatedAccount(request) == null) {
      if (!isPathExcluded(request.getRequestURI())) { // prevent re-direct loop
        response.sendRedirect("/login?next=" + request.getRequestURI()); // redirect to login page
        return;
      }
    }

    filterChain.doFilter(servletRequest, servletResponse);
  }

  @Override
  public void destroy() {
    // do nothing
  }

  private boolean isPathExcluded(String path) {
    if (path != null) {
      for (String s : EXCLUDED_PATHS) {
        if (path.startsWith(s)) {
          return true;
        }
      }
    }
    return false;
  }

  private final String[] EXCLUDED_PATHS = new String[]{
     "/public", "/error", // springboot filters
     "/favicon.ico", "/login", "/assets", "/register", "/forgot", "/password", "/verify", "/change", "/oauth" // stormpath filters
  };
}
