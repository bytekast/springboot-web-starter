package com.bytekast.app;

import org.springframework.boot.context.embedded.ConfigurableEmbeddedServletContainer;
import org.springframework.boot.context.embedded.EmbeddedServletContainerCustomizer;
import org.springframework.stereotype.Component;

/**
 * @author Rowell Belen
 */
@Component
public class CustomizationBean implements EmbeddedServletContainerCustomizer {

  private static int PORT = 8000;

  @Override
  public void customize(ConfigurableEmbeddedServletContainer container) {
    try {
      // Use PORT provided in the System Environment
      int port = Integer.parseInt(System.getenv("PORT"));
      container.setPort(port);
      System.out.println("Using port: " + port);
    }
    catch (Exception e) {
      // Use default port
      System.out.println("Using port: " + PORT);
      container.setPort(PORT);
    }
  }
}
