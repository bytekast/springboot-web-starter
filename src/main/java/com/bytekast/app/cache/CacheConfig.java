package com.bytekast.app.cache;

import net.spy.memcached.AddrUtil;
import net.spy.memcached.ConnectionFactoryBuilder;
import net.spy.memcached.MemcachedClient;
import net.spy.memcached.auth.AuthDescriptor;
import net.spy.memcached.auth.PlainCallbackHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author Rowell Belen
 */
@Configuration
public class CacheConfig {

  @Bean
  public MemcachedClient memcachedClient() throws Exception {
    AuthDescriptor ad = new AuthDescriptor(new String[]{"PLAIN"},
       new PlainCallbackHandler(System.getenv("MEMCACHEDCLOUD_USERNAME"), System.getenv("MEMCACHEDCLOUD_PASSWORD")));

    MemcachedClient mc = new MemcachedClient(
       new ConnectionFactoryBuilder()
          .setProtocol(ConnectionFactoryBuilder.Protocol.BINARY)
          .setAuthDescriptor(ad).build(),
       AddrUtil.getAddresses(System.getenv("MEMCACHEDCLOUD_SERVERS")));
    return mc;
  }
}
