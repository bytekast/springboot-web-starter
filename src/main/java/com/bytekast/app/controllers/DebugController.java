package com.bytekast.app.controllers;

import com.bytekast.app.security.interceptors.AuthorizationRequired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Rowell Belen
 */
@Component
@RestController
@RequestMapping("/debug")
public class DebugController {

  @AuthorizationRequired
  @RequestMapping(value = "/headers", method = RequestMethod.GET)
  public Map getHeaders(HttpServletRequest request) {

    Map<String, String> map = new HashMap<>();

    Enumeration headerNames = request.getHeaderNames();
    while (headerNames.hasMoreElements()) {
      String key = (String) headerNames.nextElement();
      String value = request.getHeader(key);
      map.put(key, value);
    }

    return map;
  }
}
