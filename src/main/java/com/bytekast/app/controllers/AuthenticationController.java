package com.bytekast.app.controllers;

import com.bytekast.app.security.interceptors.AuthorizationRequired;
import com.bytekast.app.security.utils.StormpathUtils;
import com.stormpath.sdk.account.Account;
import com.stormpath.sdk.api.ApiKey;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

/**
 * @author Rowell Belen
 */
@RestController
@RequestMapping(value = "/auth")
public class AuthenticationController {

  @Autowired
  private StormpathUtils stormpathUtils;

  @AuthorizationRequired
  @RequestMapping(value = "/apiKeys", method = RequestMethod.POST)
  public List<Properties> postApiKeys(HttpServletRequest request, HttpServletResponse response) {

    Account account = stormpathUtils.getAuthenticatedAccount(request);

    //Prepares the response back to the caller
    response.setStatus(HttpServletResponse.SC_OK);
    response.setContentType("application/json");

    List list = new ArrayList();
    if (account.getApiKeys() == null) {
      account.createApiKey();
    }

    Iterator<ApiKey> it = account.getApiKeys().iterator();
    while (it.hasNext()) {
      ApiKey apiKey = it.next();
      Properties properties = new Properties();
      properties.put("id", apiKey.getId());
      properties.put("secret", apiKey.getSecret());
      properties.put("status", apiKey.getStatus().name());
      list.add(properties);
    }

    return list;
  }
}
