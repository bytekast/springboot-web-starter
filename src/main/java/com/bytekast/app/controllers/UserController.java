package com.bytekast.app.controllers;

import com.bytekast.app.models.Todo;
import com.bytekast.app.security.interceptors.AuthorizationRequired;
import com.bytekast.app.security.utils.StormpathUtils;
import com.stormpath.sdk.account.Account;
import com.stormpath.sdk.impl.account.DefaultAccount;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * @author Rowell Belen
 */
@RestController
@RequestMapping(value = "/api/users")
public class UserController {

  @Autowired
  private StormpathUtils stormpathUtils;

  @RequestMapping(value = "/name", method = RequestMethod.GET)
  public
  @ResponseBody
  String getName(HttpServletRequest request) {

    Account account = stormpathUtils.getAuthenticatedAccount(request);
    if (account != null) {
      //do something with the account
      return account.getFullName();
    }

    return "Anonymous";
  }

  @AuthorizationRequired
  @RequestMapping(value = "/current", method = RequestMethod.GET)
  public Map getCurrent(HttpServletRequest request) {

    Map<String, Object> map = new HashMap<>();
    Account account = stormpathUtils.getAuthenticatedAccount(request);
    if (account != null) {
      DefaultAccount defaultAccount = (DefaultAccount) account;
      Iterator<String> it = defaultAccount.getPropertyNames().iterator();
      while (it.hasNext()) {
        String prop = it.next();
        map.put(prop, defaultAccount.getProperty(prop));
      }
    }

    return map;
  }

  @AuthorizationRequired
  @RequestMapping(value = "/todos", method = RequestMethod.POST)
  public void saveTodos(final HttpServletRequest request, @RequestBody final List<Todo> todoList) {
    Account account = stormpathUtils.getAuthenticatedAccount(request);
    stormpathUtils.saveTodos(account, todoList);
  }

  @AuthorizationRequired
  @RequestMapping(value = "/todos", method = RequestMethod.GET)
  public List<Todo> getTodos(final HttpServletRequest request) {
    Account account = stormpathUtils.getAuthenticatedAccount(request);
    return stormpathUtils.getTodos(account);
  }
}
