/*global define*/
'use strict';

define([
  'angular'
], function (angular) {
  return ['$scope', '$location', 'todoStorage', 'filterFilter', '$http',
    function ($scope, $location, todoStorage, filterFilter, $http) {

      /*
      $http({
        method: "POST",
        url: '/oauth/token',
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        },
        transformRequest: function (obj) {
          var str = [];
          for (var p in obj) {
            str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
          }
          return str.join("&");
        },
        data: {
          'grant_type': 'password',
          'username': '${USER_NAME}',
          'password': '${PASSWORD}'
        }
      }).success(function (data, status, headers, config) {
        console.log(data);

        $scope.accessToken = data['access_token'];

      }).error(function (data, status, headers, config) {
        console.log(data);
      });
      */

      $scope.info = function () {
        $http({
          method: "GET",
          url: '/api/users/current',
          headers: {
            //'Authorization': 'Bearer ' + $scope.accessToken
          }
        }).success(function (data, status, headers, config) {
          console.log(data);
        }).error(function (data, status, headers, config) {
          console.log(data);
        });

      }
    }
  ];
});
