/*global define*/
'use strict';

/**
 * Services that persists and retrieves TODOs from localStorage.
 */

define([
  'angular'
], function (angular) {
  var moduleName = 'TodoStorageModule';
  angular
    .module(moduleName, [])
    .factory('todoStorage', ['$http', function ($http) {
      //var STORAGE_ID = 'todos-angularjs-requirejs';

      return {
        get: function () {
          //return JSON.parse(localStorage.getItem(STORAGE_ID) || '[]');
          var promise = $http({
            method: "GET",
            url: '/api/users/todos'
          });
          return promise;
        },

        put: function (todos) {
          //localStorage.setItem(STORAGE_ID, JSON.stringify(todos));
          $http({
            method: "POST",
            url: '/api/users/todos',
            data: todos
          }).success(function (data, status, headers, config) {
            console.log('saved', data);
          }).error(function (data, status, headers, config) {
            console.log(data);
          });
        }
      };
    }]);
  return moduleName;
});
