/*global require*/
'use strict';

require([
  'angular',
  'angular-ui-router',
  'stormpath-sdk'
], function (angular) {
  require([
    'controllers/auth',
    'controllers/todo',
    'directives/todoFocus',
    'directives/todoEscape',
    'services/todoStorage'
  ], function (authCtrl, todoCtrl, todoFocusDir, todoEscapeDir, todoStorageSrv) {
    angular
      .module('todomvc', ['ui.router', 'stormpath', todoFocusDir, todoEscapeDir, todoStorageSrv])
      .controller('TodoController', todoCtrl)
      .controller('AuthController', authCtrl);
    angular.bootstrap(document, ['todomvc']);
  });
});
