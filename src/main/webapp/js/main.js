/*global require*/
'use strict';

require.config({
  paths: {
    angular: '../bower_components/angular/angular',
    'angular-ui-router': '../bower_components/angular-ui-router/release/angular-ui-router',
    'stormpath-sdk': '../bower_components/stormpath-sdk-angularjs/dist/stormpath-sdk-angularjs',
    'stormpath-sdk-tpls': '../bower_components/stormpath-sdk-angularjs/dist/stormpath-sdk-angularjs.tpls'
  },
  shim: {
    angular: {
      exports: 'angular'
    },
    'angular-ui-router': {
      deps: ['angular']
    },
    'stormpath-sdk': {
      deps: ['angular']
    },
    'stormpath-sdk-tpls': {
      deps: ['angular-ui-router', 'stormpath-sdk']
    }
  },
  deps: ['app']
});
