[![Build Status](https://travis-ci.org/bytekast/springboot-web-starter.svg?branch=master)](https://travis-ci.org/bytekast/springboot-web-starter)

# springboot-web-starter

Make sure you have [Maven](https://maven.apache.org/) and [Foreman](https://github.com/ddollar/foreman) installed.

Register for a [Stormpath](https://stormpath.com) account and modify the `.env` file and update the Stormpath settings.

Run Locally: `mvn package && foreman start web`

The default address is: http://localhost:8000

The following default management endpoints are available:
- http://localhost:8000/manage/health
- http://localhost:8000/manage/metrics
- http://localhost:8000/manage/info

To deploy in Heroku:
- Install [Heroku Toolbelt](https://toolbelt.heroku.com)
- Run `heroku create` in the project directory
- Run `heroku config:push --overwrite --interactive` to transfer configuration from `.env` to heroku environment
- Run `git push heroku master` to build and deploy
- To view the app, run `heroku open`
